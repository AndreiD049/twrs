#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use std::{path::{Path, PathBuf}, fs::{create_dir_all, copy}};

use api::serve;
use constants::{DATA, WIKIS, TEMPLATES};
use tauri;

mod api;
mod constants;
mod utils;

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn get_local_address() -> String {
    String::from("localhost")
}

fn init_app(base_dir: PathBuf) {
    let data_path = base_dir.join(DATA);
    if !Path::exists(&data_path) {
        create_dir_all(&data_path).expect("Couldn't create config folder");
    }
    let wikis_path = data_path.join(WIKIS);
    if !Path::exists(&wikis_path) {
        create_dir_all(&wikis_path).expect("Couldn't create config folder");
    }
    let main_template = base_dir.join(TEMPLATES).join("index.html");
    println!("{}", main_template.to_str().unwrap());
    if !Path::exists(&data_path.join("index.html")) {
        if !Path::exists(&main_template) {
            panic!("Main template not found")
        }
        copy(&main_template, &data_path.join("index.html")).unwrap();
    }
}

fn main() {
    tauri::Builder::default()
        .setup(|app| {
            init_app(app.path_resolver().resource_dir().unwrap());
            let base_dir = app.path_resolver().resource_dir().unwrap();
            tauri::async_runtime::spawn(async move {
                serve(base_dir, 3007).await
            });
            Ok(())
        })
        .invoke_handler(tauri::generate_handler![get_local_address])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
