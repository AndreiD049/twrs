use std::{
    fs::{copy, read_dir},
    net::{SocketAddr},
    path::PathBuf,
};

use dav_server::warp::dav_dir;
use serde::{Deserialize, Serialize};
use tauri::{http::header::{CACHE_CONTROL, EXPIRES}};
use warp::{
    cors::Builder, filters::BoxedFilter, http::HeaderValue, hyper::HeaderMap, reply, Filter, Reply,
};

use crate::{
    constants::{DATA, TEMPLATES, WIKIS},
    utils::open,
};

#[derive(Deserialize, Serialize)]
struct CreateWiki {
    name: String,
    template: String,
}

#[derive(Deserialize, Serialize)]
struct OpenWiki {
    folder: String,
    name: String,
    origin: String,
}

fn create_wiki(body: CreateWiki, templates_dir: &PathBuf, wikis_dir: &PathBuf) -> impl Reply {
    let path = templates_dir.join("empty.html");
    let local_dir = wikis_dir.join(format!("{}.html", body.name));
    copy(&path, &local_dir).unwrap();
    reply()
}

fn get_html_files_in_folder(folder: &PathBuf) -> Vec<String> {
    let mut html_files: Vec<String> = Vec::new();

    if folder.is_dir() {
        for entry in read_dir(folder).unwrap() {
            let path = entry.unwrap().path();
            if path.is_file() {
                if let Some(ext) = path.extension() {
                    if ext == "html" {
                        if let Some(file_name) = path.file_name() {
                            if let Some(file_name_str) = file_name.to_str() {
                                html_files.push(file_name_str.to_string());
                            }
                        }
                    }
                }
            } else if path.is_dir() {
                let mut nested_files = get_html_files_in_folder(&path);
                html_files.append(&mut nested_files);
            }
        }
    }

    html_files
}

fn get_cors() -> Builder {
    warp::cors()
        .allow_any_origin()
        .allow_headers(vec![
            "User-Agent",
            "Sec-Fetch-Mode",
            "Referer",
            "Origin",
            "Access-Control-Request-Method",
            "Access-Control-Request-Headers",
            "Accept",
            "Content-Type",
            "X-Requested-With",
        ])
        .allow_header("*")
        .allow_methods(vec!["GET", "POST", "DELETE", "OPTIONS"])
}

fn api_route(base_dir: PathBuf) -> BoxedFilter<(impl Reply,)> {
    let templates_dir = base_dir.join(TEMPLATES);
    let data_dir = base_dir.join(DATA).join(WIKIS);
    let api = warp::path("api");

    // Create a new wiki
    let create_wiki = warp::post()
        .and(warp::path("create-wiki"))
        .and(warp::body::json())
        .map(move |body: CreateWiki| create_wiki(body, &templates_dir, &data_dir));

    // Open wiki in browser
    // /api/open-wiki-browser
    let open_wiki_browser = warp::post()
        .and(warp::path("open-wiki-browser"))
        .and(warp::body::json())
        .map(|body: OpenWiki| {
            open(format!("{}/{}/{}", body.origin, body.folder, body.name).as_str()).unwrap();
            reply()
        });

    let data_dir = base_dir.join(DATA).join(WIKIS);
    let get_wikis = warp::get()
        .and(warp::path("get-wikis"))
        .map(move || {
            let paths = get_html_files_in_folder(&data_dir);
            reply::json(&paths)
        });

    let api = api.and(
        create_wiki
            .or(open_wiki_browser)
            .or(get_wikis)
    );
    api.boxed()
}

// Receiving the directory of the executable and the port
pub async fn serve(dir: PathBuf, port: u16) {
    let mut headers = HeaderMap::new();
    headers.insert(
        CACHE_CONTROL,
        HeaderValue::from_static("no-store, must-revalidate"),
    );
    headers.insert(EXPIRES, HeaderValue::from_static("0"));

    env_logger::init();
    let addr: SocketAddr = ([127, 0, 0, 1], port).into();
    let data_dir = dir.join(DATA);
    let warpdav = dav_dir(data_dir, true, true).with(warp::reply::with::headers(headers));
    warp::serve(
        api_route(dir)
            .with(get_cors())
            .or(warpdav)
            .with(warp::log("info")),
    )
    .run(addr)
    .await;
}
