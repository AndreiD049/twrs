use tauri::api::process::Command;

pub fn open(path: &str) -> Result<(), String> {
    if cfg!(target_os = "windows") {
        Command::new("cmd")
            .args(["/C", "start", path])
            .spawn()
            .map(|_| ())
            .map_err(|e| e.to_string())
    } else {
        Command::new("sh")
            .args(["-c", "xdg-open", path])
            .spawn()
            .map(|_| ())
            .map_err(|e| e.to_string())
    }
}
