/*\
title: $:/plugins/andreid049/twrs-api/create-wiki
type: application/javascript
module-type: macro

I'm just a test macro
\*/
(function () {
  "use strict";

  const TEMP_TIDDLER = "$:/temp/NewDatabaseName";
  const TEMP_ICON_TIDDLER = "$:/temp/NewDatabaseIcon";

  exports.name = "create-wiki";
  exports.params = [{ name: "name" }, { name: "icon" }];
  exports.run = async function (name, icon) {
    const reply = await fetch(`${location.origin}/api/create-wiki`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
      },
      body: JSON.stringify({
        name: name,
        template: "test",
      }),
    });
    if (reply.status === 200) {
      $tw.wiki.addTiddler({
        title: name,
        tags: ["Database"],
        created: new Date(),
        modified: new Date(),
        type: "text/vnd.tiddlywiki",
        icon: icon,
      });
      console.log(name);
      $tw.wiki.setText(TEMP_TIDDLER, "text", null, "");
      $tw.wiki.setText(TEMP_ICON_TIDDLER, "text", null, "");
    }
  };
})();
