/*\
title: $:/plugins/andreid049/twrs-api/startup.js
type: application/javascript
module-type: startup

Startup initialisation

\*/
(function () {
  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  // Export name and synchronous status
  exports.name = "twrs-api";
  exports.platforms = ["browser"];
  exports.after = ["startup"];
  exports.before = ["render"];
  exports.synchronous = true;

  exports.startup = function () {
    if ($tw.node) return;
    
    const lib = require('$:/plugins/andreid049/twrs-api/lib');
    lib.syncWikis();

    $tw.rootWidget.addEventListener("twrs-open-wiki", function (event) {
      const { folder, name } = event.paramObject;
      openWikiNewWindow(folder, name);
    });

    $tw.rootWidget.addEventListener("twrs-open-wiki-browser", function (event) {
      const { folder, name } = event.paramObject;
      openWikiInBrowser(folder, name).catch((err) => console.error(err));
    });
  };
})();

function openWikiNewWindow(folder, name) {
  if (folder && name) {
    if (!name.endsWith(".html")) {
      name += ".html";
    }
    window.open(`/${folder}/${name}`, "_blank", "noreferrer,noopener");
  }
}

async function openWikiInBrowser(folder, name) {
  if (!folder || !name) return;
  if (!name.endsWith(".html")) {
    name += ".html";
  }
  await fetch(`${location.origin}/api/open-wiki-browser`, {
    method: "POST",
    body: JSON.stringify({
      folder,
      name,
      origin: location.origin,
    }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json;charset=UTF-8",
    },
  });
}
