/*\
title: $:/plugins/andreid049/twrs-api/lib
type: application/javascript
module-type: library

Utility functions
\*/
(function () {
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json;charset=UTF-8",
  };
  exports.headers = headers;
  exports.syncWikis = async () => {
    const response = await fetch("http://localhost:3007/api/get-wikis", {
      method: "GET",
      headers: headers,
    });
    const files = await response.json();
    const dbNames = files.map((f) => f.replace('.html', ''));
    const tiddlers = $tw.wiki.filterTiddlers("[tag[Database]]");
    const tiddlersSet = new Set(tiddlers);
    // See if there are any new Databases
    dbNames.forEach((localDb) => {
        if (!tiddlersSet.has(localDb)) {
            createDatabase(localDb, '🆕');
        }
    });
    // Delete tiddlers that no longer have a file in the background
    const dbSet = new Set(dbNames);
    tiddlers.forEach((tiddler) => {
        if (!dbSet.has(tiddler)) {
            $tw.wiki.deleteTiddler(tiddler);
        }
    });
  };
})();

function createDatabase(name, icon) {
  $tw.wiki.addTiddler({
    title: name,
    tags: ["Database"],
    created: new Date(),
    modified: new Date(),
    type: "text/vnd.tiddlywiki",
    icon: icon,
  });
}
