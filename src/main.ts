import { invoke } from "@tauri-apps/api";

window.addEventListener("DOMContentLoaded", async () => {
  try {
    let address = await invoke('get_local_address');
    location.href = `http://${address}:3007/index.html`;
  } catch (err) {
    location.href = 'http://localhost:3007/index.html';
  }
});
